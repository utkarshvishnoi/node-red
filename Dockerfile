FROM nodered/node-red

ARG HOST_UID=1001

COPY entrypoint.sh /usr/src/node-red/entrypoint.sh

USER root

RUN apk add --no-cache docker shadow

RUN usermod -u $HOST_UID node-red
RUN groupmod -n tmp ping
RUN groupmod -n ping docker
RUN groupmod -n docker tmp
RUN usermod -aG docker node-red

RUN apk del shadow && chmod +x /usr/src/node-red/entrypoint.sh

USER node-red
# Node-Red Custom Docker Image

A custom docker image with support to fetch and install a self signed root certificate.

## Build

```bash
  git clone https://gitlab.com/utkarshvishnoi25/node-red.git
  cd node-red
  docker build . -t utkarsh/node-red
```

## Authors

- [@utkarsh-vishnoi](https://www.github.com/utkarsh-vishnoi)
